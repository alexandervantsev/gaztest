﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace gazprom_test.Models
{
    public class Soft
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Name { get; set; }
    }
}