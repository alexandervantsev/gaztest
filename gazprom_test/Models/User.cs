﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace gazprom_test.Models
{
    public class User
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public String RegDate { get; set; }
    }
}