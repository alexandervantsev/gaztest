﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using gazprom_test.Models;
using System.Data;

namespace gazprom_test.Controllers
{
    [Authorize]
    public class UsersController : ApiController
    {
        MyDbContext context = new MyDbContext();
        [AllowAnonymous]
        public IEnumerable<User> GetUsers()
        {
            User user = new Models.User();
            user.Name = HttpContext.Current.Request.LogonUserIdentity.Name;
            user.RegDate = System.DateTime.Now.ToString();
            try
            {
                context.Users.Add(user);
                context.SaveChanges();
            }
            catch (System.Data.Entity.Infrastructure.DbUpdateException exc)
            {
                System.Console.Error.WriteLine("User is already in the DB");
            }
            
            return context.Users;
        }
        [AllowAnonymous]
        public User GetUser(int id)
        {
            User user;
            if (id == 0)
            {
                user = new Models.User();
                user.Name = HttpContext.Current.Request.LogonUserIdentity.Name;
            }
            else
            {
                user = context.Users.Find(id);
            }
            return user;
        }

        
        public void PostUser([FromBody]User user)
        {
            context.Users.Add(user);
            context.SaveChanges();
        }

        
        public void Put(int id, [FromBody]User user)
        {
            if (id == user.Id)
            {
                context.Entry(user).State = (System.Data.Entity.EntityState) EntityState.Modified;
                context.SaveChanges();
            }
        }

       
        public void Delete(int id)
        {
            User user = context.Users.Find(id);
            if (user != null)
            {
                context.Users.Remove(user);
                context.SaveChanges();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
