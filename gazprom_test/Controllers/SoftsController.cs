﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using gazprom_test.Models;
using System.Data;

namespace gazprom_test.Controllers
{
    [Authorize]
    public class SoftsController : ApiController
    {
        MyDbContext context = new MyDbContext();
        [AllowAnonymous]
        public IEnumerable<Soft> GetSoft()
        {
            return context.Softs.ToList();
        }

        [AllowAnonymous]
        public Soft GetSoft(int id)
        {
            Soft soft = context.Softs.Find(id); 
            return soft;
        }

       
        public void PostSoft([FromBody]Soft soft)
        {
            context.Softs.Add(soft);
            context.SaveChanges();
        }

     
        public void Put(int id, [FromBody]Soft soft)
        {
            if (id == soft.Id)
            {
                context.Entry(soft).State = (System.Data.Entity.EntityState)EntityState.Modified;
                context.SaveChanges();
            }
        }

      
        public void Delete(int id)
        {
            Soft soft = context.Softs.Find(id);
            if (soft != null)
            {
                context.Softs.Remove(soft);
                context.SaveChanges();
            }
        }       
    }
}